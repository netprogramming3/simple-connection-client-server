import socket


def main():
    host = "127.0.0.1"
    port = 12345

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((host, port))

    num = 1
    while num <= 100:
        num_received = int(client_socket.recv(1024).decode())
        print("Server:", num_received)
        num_to_send = num_received + 1
        client_socket.send(str(num_to_send).encode())
        num += 1

    client_socket.close()


if __name__ == "__main__":
    main()
