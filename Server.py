import socket


def main():
    host = "127.0.0.1"
    port = 12345

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    server_socket.listen(1)

    print("Server is listening for connections...")

    connection, addr = server_socket.accept()
    print(f"Connected to client at {addr}")

    num = 1
    while num <= 100:
        connection.send(str(num).encode())
        num_received = int(connection.recv(1024).decode())
        print("Client:", num_received)
        num = num_received + 1

    connection.close()
    server_socket.close()


if __name__ == "__main__":
    main()
